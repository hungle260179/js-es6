// khai bao bien ngay mai
const getDateTomorrow = () => {
  let today = new Date();
  return (
    today.getMonth() + "/" + (today.getDate() + 1) + "/" + today.getFullYear()
  );
};
// So sanh giua 2 ngay
const compare_dates = function (date1, date2) {
  let d1 = new Date(date1);
  let d2 = new Date(date2);

  if (d1 > d2) return 1;
  else if (d1 < d2) return -1;
  else return 0;
};
//khai bao ngay hien tai
const getDate = () => {
  let today = new Date();
  return today.getMonth() + "/" + today.getDate() + "/" + today.getFullYear();
};
// BAi 9

class Product {
  constructor(id, name, categoryId, saleDate, quylity, isDelete) {
    this.id = id;
    this.name = name;
    this.categoryId = categoryId;
    this.saleDate = saleDate;
    this.quylity = quylity;
    this.isDelete = isDelete;
  }
}

// Bai10
const listProducts = [];
function FetchlisProduct() {
  for (i = 0; i < 10; i++) {
    listProducts.push(
      new Product(
        i,
        "Product " + i,
        i,
        getDateTomorrow(),
        i + 10,
        i % 2 == 0 ? true : false
      )
    );
  }
  return listProducts;
}
FetchlisProduct();
console.log("Bai10");
console.log(listProducts);

// Bai11

// Dung es6
filterProductbyIdEs6 = (ListProduct, idProduct) => {
  if (!ListProduct || idProduct < 0) return 0;
  return ListProduct.find((x) => x.id == idProduct);
};
console.log("Bai11");
console.log(filterProductbyIdEs6(listProducts, 10));

//dung for
filterProductById = (listProduct, idProduct) => {
  let product = null;
  for (let i = 0; i < listProduct.length; i++) {
    if (listProduct[i].id == idProduct) product = listProduct[i];
  }
  return product;
};
console.log(filterProductById(listProducts, 10));
//Bai12

//dung es6
filterProductByQualityEs6 = (listProduct) => {
  if (!listProduct) return null;
  return listProduct.filter(
    (product) => product.quylity > 0 && !product.isDelete
  );
};
console.log("Bai12");
console.log(filterProductByQualityEs6(listProducts));

//dung for
filterProductByQuality = (listProduct) => {
  let list = [];
  for (let i = 0; i < listProduct.length; i++) {
    if (listProduct[i].quylity > 0 && !listProduct[i].isDelete)
      list.push(listProduct[i]);
  }
  return list;
};
console.log(filterProductByQuality(listProducts));

//Bai13

//es6
filterProductBySaleDateEs6 = (listProduct) => {
  if (!listProduct) return null;
  return listProduct.filter(
    (product) =>
      compare_dates(product.saleDate, getDate()) == 1 && !product.isDelete
  );
};
console.log("Bai13");
console.log(filterProductBySaleDateEs6(listProducts));
// dung for
filterProductBySaleDate = (listProduct) => {
  let list = [];
  for (i = 0; i < listProduct.length; i++) {
    if (
      compare_dates(listProduct[i].saleDate, getDate()) == 1 &&
      !listProduct[i].isDelete
    )
      list.push(listProduct[i]);
  }
};
console.log(filterProductBySaleDate(listProducts));

//Bai14

//reduce
totalProductEs6 = (listProduct) => {
  if (!listProduct) return null;
  return listProduct.reduce(
    (total, value) => (!value.isDelete ? total + value.quylity : total),0);// 0 gia tri khoi tao ban dau 
};
console.log("Bai14");
console.log(totalProductEs6(listProducts));

//ko dung reduce
totalProduct = (ListProduct) => {
  let total = 0;
  if (!ListProduct) return 0;
  for (let i = 0; i < ListProduct.length; i++) {
    if (!ListProduct[i].isDelete) {
      total += ListProduct[i].quylity;//+ them quantity vs gia tri total
    }
  }
  return total;
};

console.log(totalProduct(listProducts));

//Bai 15
// dung es6
isHaveProductInCategoryEs6 = (ListProduct, categoryId) => {
  if (!ListProduct || categoryId < 0) return null;
  return ListProduct.some((product) => categoryId === product.categoryId);
};
console.log("Bai15");
console.log(isHaveProductInCategoryEs6(listProducts, 9));
//dung for
isHaveProductInCategory = (ListProduct, categoryId) => {
  let listcategory = false;
  if (!ListProduct || categoryId < 0) return false;
  for (let i = 0; i < ListProduct.length; i++) {
    if (ListProduct[i].categoryId === categoryId) {
      listcategory = true;
    }
  }
  return listcategory;
};

console.log(isHaveProductInCategory(listProducts, 9));

//Bai16
// dung es6
filterArrayProductBySaleDateEs6 = (listProduct) => {
  if (!listProduct) return null;
  return listProduct
    .filter(
      (product) =>
        product.quylity > 0 && compare_dates(product.saleDate, getDate()) == 1
    )
    .map((product) => [product.id, product.name]);
};
console.log("Bai16")
console.log(filterArrayProductBySaleDateEs6(listProducts));
//dung for
 filterArrayProductBySaleDate = (listProduct) => {
  if (!listProduct)
      return null;

  let product = [];
  for (let i = 0; i < listProduct.length; i++) {
      if (listProduct[i].quylity> 0 && compare_dates(listProduct[i].saleDate, getDate()) == 1) {
          let sale = new Array(
              listProduct[i].id,
              listProduct[i].name
          )
          product.push(sale);
      }
  }
  return product;
}
console.log(filterArrayProductBySaleDate(listProducts));
